# Card Vender
A simple online card editor for use with a cardgame we are making on the OpenCards repo.

## Usage
To run card vender, simply start it up with:
```
DB_URL=<url_to_your_mongo_instance> node server/server.js
```

Optianally, you can also provide the `PORT` env variable to specify the port to listen on.