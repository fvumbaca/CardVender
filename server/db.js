const mongoose = require('mongoose');

const dbUrl = process.env.DB_URL;
if (!dbUrl) {
    console.log("No DB_URL was provided... Will not be able to connect to db...")
}

mongoose.connect(dbUrl);


const db = mongoose.connection;
db.on('error', console.error.bind(console, 'db connection error:'));
db.once('open', function() {
    console.log("Db Connected!");
});

module.exports = db;
