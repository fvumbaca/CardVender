const express = require('express');
const app = express();
const Card = require('../models/Card');

app.use(require('body-parser').json());

app.use(express.static('public'))

app.get('/api/cards', (req, res) => {
    Card.find((err, r) => {
        if (err) {
            console.log("Err: ", err);
            res.json({error:err});
        }
        else res.json(r);
    });
});

app.post('/api/cards', (req, res) => {
    const card = new Card(req.body);
    card.save((err, r) =>{
        if (err) {
            console.log("Could not save: ", err);
            res.json({status:"error", error:err});
        }
        else res.json({status:"ok", result:r});
    })
});

const port = process.env.PORT || 3000;
app.listen(port, function () {
    console.log('App listening on port ' + port)
});
