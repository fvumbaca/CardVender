const db = require('../server/db');

module.exports = db.model('Card', {
    name: {type:String, required: true},
    description: {type: String, required: true},
    cardType: {type: String, required: true, enum:["Hero", "Spell"]},
    stats: {
        strength: Number,
        magic: Number,
        tech: Number
    }
});