import m from 'mithril';

import LayoutView from './views/Layout';
m.mount(document.body, LayoutView);