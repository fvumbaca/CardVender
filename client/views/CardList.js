import m from 'mithril';
import store from '../state/store';

import CardView from './Card';

export default {
    view(vn) {
        let items = store.getState().map((card) => 
            (<div class="col-md-3"><CardView card={card}></CardView></div>)
        );
        return (<ul>
            {items}
        </ul>)
    }
}