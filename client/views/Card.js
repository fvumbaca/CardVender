import m from 'mithril';

export default {
    view(vn) {
        if (vn.attrs.card.cardType === "Hero") { // Draw a hero card
            return (<div class="card" >
                <center>
                    <span><strong>{vn.attrs.card.name}</strong></span>
                    <img class="card-image" href="#" />
                    <div class="card-stat-container" >
                        <div class="card-strength-stat">1</div>
                        <div class="card-magic-stat">1</div>
                        <div class="card-tech-stat">1</div>
                    </div>
                    <div class="card-description" >
                        {vn.attrs.card.description}
                    </div>


                </center>
            </div>);
        } else { // Draw a spell card
            return (<div class="card" >
                <center>
                    <span><strong>{vn.attrs.card.name}</strong></span>
                    <img class="card-image" href="#" />
                    <div class="card-description" >
                        {vn.attrs.card.description}
                    </div>

                    <div class="card-stat-container" >
                        <div class="card-strength-stat">1</div>
                        <div class="card-magic-stat">1</div>
                        <div class="card-tech-stat">1</div>
                    </div>

                </center>
            </div>);
        }
    }
}