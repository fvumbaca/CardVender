import m from 'mithril';

import CardListView from './CardList';

export default {
    view() {
        return (<div class="container">
            <h1>The Layout Component</h1>
            <br />
            <div class="row">
                <CardListView ></CardListView>
            </div>
        </div>);
    }
}