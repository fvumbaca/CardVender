import { createStore } from 'redux';
import { addCards } from './actionCreators';
import reducers from './reducers';

import m from 'mithril';

let store = createStore(reducers);

store.dispatch({type:"INIT"});
m.request({
    method: "GET",
    url: "/api/cards"
})
.then(function(result) {
    store.dispatch(addCards(result));
})
.catch(err=> {
    console.log("Could not retrieve all cards.", err);
});

export default store;
export var dispatch = store.dispatch;