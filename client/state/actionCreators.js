export function addCard(card) {
    return {
        type: "ADD_CARD",
        payload: card
    }
}
export function addCards(cards) {
    return {
        type: "ADD_CARDS",
        payload: cards
    }
}