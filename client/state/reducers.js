
export default function(state = [], action) {
    switch(action.type) {
        case 'ADD_CARD':
            state.push(action.payload);
            return state
        case 'ADD_CARDS':
            action.payload.map(c=>state.push(c));
            return state
        default:
            return [];
    }
}